#include <stdio.h>
#include <stdlib.h>

int main()
{
    int option;
    printf("1: coder un fichier\n2: decoder un fichier\nChoix :");
    scanf("%d", &option);

    while(option < 1 || option > 2) {
        printf("ERREUR ! Veuillez choisir une valeur entre celles ci : \n1: coder un fichier\n2: decoder un fichier\nChoix :");
        scanf("%d", &option);
    }

    if(option == 1)
    {
        printf("On va coder !");
    } else if (option == 2)
    {
        printf("On va d�coder !");
    }
    return 0;
}
